# WHitelist by Regex Plugin for bunkerized nginx


## Install

```shell
git clone https://gitlab.com/the-foundation/bunkerized-nginx-whitelistregex.git
```

## Settings

- `` : whether to log requests or not

## Compose example

```yaml
version: '3'
services:

  mywww:
    image: bunkerity/bunkerized-nginx
    ports:
      - 80:8080
    volumes:
      - ./web-files:/www:ro
      - ./bunkerized-nginx-whitelistregex:/plugins/whitelistregex:ro
    environment:
      - SERVER_NAME=www.website.com # replace with your domain
      - USE_CLIENT_CACHE=yes
      - USE_GZIP=yes
      - REMOTE_PHP=myphp
      - REMOTE_PHP_PATH=/app

  myphp:
    image: php:fpm
    restart: always
    volumes:
      - ./web-files:/app
```
---


<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/bunkerized-nginx-whitelistregex/README.md/logo.jpg" width="480" height="270"/></div></a>
