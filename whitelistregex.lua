local M		= {}
local logger	= require "logger"
function whitelistSplit(s, delimiter)
    result = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        if match ~= "/" then
            table.insert(result, match);
        end
    end
    return result;
end

local dolog = "false"

function M.init ()
		logger.log(ngx.ERR, "WHITELISTREGX", "** NOT AN ERROR ** loading WHITELIST REGEX PLUGIN")
                local shallilog, flags = ngx.shared.plugins_data:get("whitelistregex_LOG_WHITELIST_REGEX")
                if shallilog == "true" then
                   dolog="true"
                end
                whitelistlogenv=""
                f = assert (io.popen ("printenv LOG_WHITELIST_REGEX"))
                for line in f:lines() do
                whitelistlogenv=whitelistlogenv..line
                end -- for loop
                f:close()
                if whitelistlogenv == "true" then
                   dolog="true"
                end

		logger.log(ngx.ERR, "WHITELISTREGX", "** NOT AN ERROR ** WHITELIST REGEX Log enabled: " .. dolog)

                whitelistenv=""
                f = assert (io.popen ("printenv WHITELIST_URI"))
                for line in f:lines() do
                whitelistenv=whitelistenv..line
                end -- for loop
                f:close()
		logger.log(ngx.ERR, "WHITELISTREGX read env:", whitelistenv)
                whitelist_uri=whitelistSplit(whitelistenv ," ")
                s=" Loaded WHITELIST PREFIXES:\n"

                for k, v in pairs(whitelist_uri) do
                    s = s .. k .. ":" .. v .. "\n" -- concatenate key/value pairs, with a newline in-between
                end
		logger.log(ngx.ERR, "WHITELISTREGX", s)

		logger.log(ngx.ERR, "WHITELISTREGX", "** NOT AN ERROR ** loaded WHITELIST_URI")
                
	return true
end

function M.check ()
-- check if URI is whitelisted
                for k, v in pairs(whitelist_uri) do
                    if "/" ~= v and string.match(ngx.var.request_uri, "^" .. v ) then
                      if dolog == "true" then
                        logger.log(ngx.NOTICE, "WHITELISTREGX", "URI " .. v .. " is whitelisted - subURI hit")
                      end
                    ngx.exit(ngx.OK)
                    end
                end
end
return M
